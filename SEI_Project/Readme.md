Used APIs:
 - NSUserDefaults
 - CoreData
 - CoreLocation
 - MapKit
 - UIImagePickerView
 - KeyChain

Tested Devices:
 - iPhone 6s (iOS 10.3.2)

Limitations:
 - No landscape available
 - TestDevice need a Camera

Good to Know:
 - in TableView (2nd Tab) swipe a Cell left to get Cell-Actions
 - if you register the (existing) User will be overwritten with all its data. 

Known-Issues:
 - in TableView (2nd Tab): if you want to select the cell-action „Pic“ the App will crash
	Image can be successfully stored in CoreData but if I want to use the variable which is from
	NSData to Data converted the variable has a nil-value. If I inspect the variable: expression
	produced error: error: /var/folders/wk/sssd0vyx6nq16fqcfmjfw19m0000gp/T/./lldb/3351/expr1.swift:1:65: error:
	 use of undeclared type 'Foundation'
	Swift._DebuggerSupport.stringForPrintObject(Swift.UnsafePointer<Foundation.Data>(bitPattern: 0x103560530)!.pointee)
