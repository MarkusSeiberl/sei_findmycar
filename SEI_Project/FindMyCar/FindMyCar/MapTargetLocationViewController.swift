//
//  MapTargetLocationViewController.swift
//  FindMyCar
//
//  Created by Admin on 08.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapTargetLocationViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var map: MKMapView!
    
    var long:String = String()
    var lat:String = String()
    
    let locationManager = CLLocationManager()
    let annotation = MKPointAnnotation()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        let mapspan: MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let mylocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(lat)!, Double(long)!)
        let mapRegeion: MKCoordinateRegion = MKCoordinateRegionMake(mylocation, mapspan)
        map.setRegion(mapRegeion, animated: true)
        
        self.map.showsUserLocation = true
        
        lat = String(location.coordinate.latitude)
        long = String(location.coordinate.longitude)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        annotation.coordinate = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
        map.addAnnotation(annotation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
