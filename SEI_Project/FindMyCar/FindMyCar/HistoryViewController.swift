//
//  HistoryViewController.swift
//  FindMyCar
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var historyList: UITableView!
    
    var loctaionArray: [Location] = [Location]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        historyList.delegate = self
        historyList.dataSource = self
        
        DispatchQueue.main.async {
            self.historyList.reloadData()
        }
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        loctaionArray = [Location]()
        
        let request:NSFetchRequest<Location> = Location.fetchRequest()
        
        do {
            let fetchresult = try (UIApplication.shared.delegate as! AppDelegate).managedContext.fetch(request)
            for item in fetchresult {
                loctaionArray.append(item)
            }
        }
        catch let error as NSError {
            print("Fetch failed: \(error.localizedDescription)")
        }
        loctaionArray = loctaionArray.reversed()
        historyList.reloadData()
    }

    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loctaionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as? HistoryTableViewCell
        
        let formatter_date = DateFormatter()
        let formatter_time = DateFormatter()
        formatter_date.dateFormat = "dd.MM.yy"
        formatter_time.dateFormat = "HH:mm"
        
        cell?.lblDate.text = formatter_date.string(from: loctaionArray[indexPath.row].date! as Date)
        cell?.lblTime.text = formatter_time.string(from: loctaionArray[indexPath.row].date! as Date)
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let mapAction = UITableViewRowAction(style: .normal, title: "Map") { (rowAction, indexPath) in
            let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "Map") as! MapTargetLocationViewController
            vc1.lat = self.loctaionArray[indexPath.row].latitude! as String
            vc1.long = self.loctaionArray[indexPath.row].longitude! as String
            self.present(vc1, animated: true, completion: nil)
        }
        mapAction.backgroundColor = UIColor.gray
        
        let picAction = UITableViewRowAction(style: .normal, title: "Pic") { (rowAction, indexPath) in
            let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "Pic") as! PictureViewController
            let picData: Data = self.loctaionArray[indexPath.row].picture! as Data
            vc2.pictureView.image = UIImage(data: (picData as Data))
            //vc2.pictureView.image =  UIImage(data: picData as Data)
            //vc.pictureView.image = UIImage(data: self.loctaionArray[indexPath.row].picture! as Data)
            self.present(vc2, animated: true, completion: nil)
        }
        picAction.backgroundColor = UIColor.lightGray
        
        return [mapAction,picAction]
    }
    


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (IBAction)pushMyNewViewController
            {
                MyNewViewController *myNewVC = [[MyNewViewController alloc] init];
                
                // do any setup you need for myNewVC
                
                [self presentModalViewController:myNewVC animated:YES];
    }*/
}
