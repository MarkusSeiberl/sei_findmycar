//
//  LogInViewController.swift
//  FindMyCar
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class LogInViewController: UIViewController {

    
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    
    var userArray: [String] = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        btnLogIn.layer.cornerRadius = 10
        btnRegister.layer.cornerRadius = 10
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        let keyChain = KeychainSwift()
        
        
        if txtPassword.text == "" || txtUserName.text == "" {
            let alert = UIAlertController (
                title: "Error",
                message: "Please enter Username and Pasword",
                preferredStyle: UIAlertControllerStyle.alert
            )
            
            alert.addAction(UIAlertAction(
                title: "OK",
                style: .cancel,
                handler: { (action:UIAlertAction) ->Void in
                })
            )
            present(alert, animated:true, completion:nil)
            
            return false
        }
        
        if identifier == "logIn" {
            if let password = keyChain.get("loginPassword"), let username = keyChain.get("loginName") {
                if password == txtPassword.text! && username == txtUserName.text! {
                    return true
                }
                else {
                    let alert = UIAlertController (
                        title: "Error",
                        message: "Username or Pasword incorrect",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    
                    alert.addAction(UIAlertAction(
                        title: "Try Again",
                        style: .cancel,
                        handler: { (action:UIAlertAction) ->Void in
                    })
                    )
                    present(alert, animated:true, completion:nil)
                    return false
                }
            }
            else {
                let alert = UIAlertController (
                    title: "Error",
                    message: "Please Register first.",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                
                alert.addAction(UIAlertAction(
                    title: "Ok",
                    style: .cancel,
                    handler: { (action:UIAlertAction) ->Void in
                })
                )
                present(alert, animated:true, completion:nil)
                return false
            }
            
        }
        else if identifier == "register" {
            keyChain.set(txtUserName.text!, forKey: "loginName")
            keyChain.set(txtPassword.text!, forKey: "loginPassword")
            self.deleteAllData(entity: "Location")
            return true
        }
        
        return false
    }
    
    
    func deleteAllData(entity: String)
    {
        
        /*let fetchRequest = NSFetchRequest<Location>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try (UIApplication.shared.delegate as! AppDelegate).managedContext.fetch(fetchRequest)
            
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as NSManagedObject
                (UIApplication.shared.delegate as! AppDelegate).managedContext.delete(managedObjectData)
            }
            (UIApplication.shared.delegate as! AppDelegate).managedContext.save(nil)
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }*/
        
        
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let delAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "Location"))
        do {
            try managedContext.execute(delAllReqVar)
        }
        catch {
            print(error)
        }
    }
    
    //close keyboard
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
