//
//  MapViewController.swift
//  FindMyCar
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreData

class MapViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var saveLocation: UIButton!
    
    var long:String = String()
    var lat:String = String()
    
    let date = Date()
    let locationManager = CLLocationManager()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        let mapspan: MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let mylocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let mapRegeion: MKCoordinateRegion = MKCoordinateRegionMake(mylocation, mapspan)
        map.setRegion(mapRegeion, animated: true)
        
        self.map.showsUserLocation = true
        
        lat = String(location.coordinate.latitude)
        long = String(location.coordinate.longitude)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        saveLocation.layer.cornerRadius = 0.05 * saveLocation.bounds.size.width
        
    }
    
    @IBAction func saveLocation(_ sender: Any) {
        /*let setLoc = NSEntityDescription.insertNewObject(forEntityName: "Location", into: (UIApplication.shared.delegate as!AppDelegate).managedContext) as! Location
        setLoc.latitude = lat
        setLoc.longitude = long
        setLoc.date = date as NSDate
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()*/
            
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCamera" {
            if let saveLocation = segue.destination as? CameraViewController_2 {
                saveLocation.lat = lat
                saveLocation.long = long
            }
        }
    }
}
