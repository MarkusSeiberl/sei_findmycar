//
//  CameraViewController_2.swift
//  FindMyCar
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import CoreLocation

class CameraViewController_2: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {

    
    @IBOutlet weak var pickedImage: UIImageView!
    
    var long:String = String()
    var lat:String = String()
    
    let date = Date()
    let locationManager = CLLocationManager()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defaults.set("0", forKey: "Opend")
        defaults.synchronize()
    }

    override func viewDidAppear(_ animated: Bool) {
        if self.pickedImage.image != nil {
             savePicture()
        }
        else if UserDefaults.standard.value(forKey: "Opend") as! String == "1" {
            self.dismiss(animated: true, completion: nil)
        }
        else {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                
                //store nsuserdefaults
                //let defaults = UserDefaults.standard
                defaults.set("1", forKey: "Opend")
                defaults.synchronize()
                
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func savePicture() {
        if let imageData = UIImageJPEGRepresentation(pickedImage.image!, 0.6) {
            let compressedJPEGImage = UIImage(data: imageData)
            UIImageWriteToSavedPhotosAlbum(compressedJPEGImage!, nil, nil, nil)
            
            defaults.set("0", forKey: "Opend")
            defaults.synchronize()
        }
        
        //save in coredata
        saveInCoreData()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!){
        pickedImage.image = image
        self.dismiss(animated: true, completion: nil);
    }
    
    
    func saveInCoreData() {
        let img = UIImageJPEGRepresentation(pickedImage.image!,1)
        
        let setLoc = NSEntityDescription.insertNewObject(forEntityName: "Location", into: (UIApplication.shared.delegate as!AppDelegate).managedContext) as! Location
        setLoc.latitude = lat
        setLoc.longitude = long
        //setLoc.picture = UIImagePNGRepresentation(pickedImage.image!)! as NSData;
        setLoc.picture = img! as NSData
        setLoc.date = date as NSDate
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
}
