//
//  FindMyCarUnitTests.swift
//  FindMyCarUnitTests
//
//  Created by Admin on 10.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import XCTest
@testable import FindMyCar

class FindMyCarUnitTests: XCTestCase {
    
    var SUT: LogInViewController!
    
    override func setUp() {
        super.setUp()
        
        SUT = UIStoryboard(name: "Main", bundle: Bundle(for: LogInViewController.self)).instantiateViewController(withIdentifier:"login") as! LogInViewController
        let _ = SUT.view
    }
    
    override func tearDown() {
        SUT = nil
        super.tearDown()
    }
    
    func testLogin() {
        
        let identifiers = segues(ofViewController: SUT)
        XCTAssertEqual(identifiers.count, 2, "Segue count should equal two.")
        XCTAssertTrue(identifiers.contains("logIn"), "Segue LogIn should exist.")
        XCTAssertTrue(identifiers.contains("register"), "Segue Register should exist.")
    }

    func segues(ofViewController viewController: UIViewController) -> [String] {
        let identifiers = (viewController.value(forKey: "storyboardSegueTemplates") as? [AnyObject])?.flatMap({ $0.value(forKey: "identifier") as? String }) ?? []
        return identifiers
    }
}
